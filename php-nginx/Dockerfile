FROM gcr.io/google_appengine/base

# persistent / runtime deps
RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y --no-install-recommends \
    autoconf \
    automake \
    build-essential \
    cmake \   
    cron \
    curl \
    gcc \
    gettext \
    git \
    libass-dev \
    libbz2-1.0 \
    libfreetype6-dev \
    libgpac-dev \
    libicu52 \
    libmcrypt4 \
    libmemcached11 \
    libmemcachedutil2 \
    libpcre3 \
    libpcre3-dev \
    libicu-dev \
    libjpeg-dev \
    libmcrypt-dev \
    libmp3lame-dev \
    libpng12-0 \
    libpng12-dev \
    libpq5 \
    libreadline6 \
    librecode0 \
    libsqlite3-0 \
    libssl-dev \
    libtool \
    libtheora-dev \
    libvorbis-dev \	   
    libvdpau-dev \	 
    libxml2 \
    libxslt1.1 \
    logrotate \
    make \
    mercurial \
    pkg-config \	
    php5-dev \ 
    php5-mysql \
    php5-memcached \
    php5-curl \
    supervisor \
    texi2html \ 
    wget \
    yasm  \ 
    zlib1g \
    zlib1g-dev 
    

ENV NGINX_DIR=/usr/local/nginx \
    PHP_DIR=/usr/local/php \
    PHP56_DIR=/usr/local/php56 \
    PHP7_DIR=/usr/local/php7 \
    LOG_DIR=/var/log/app_engine \
    APP_DIR=/app \
    NGINX_USER_CONF_DIR=/etc/nginx/conf.d \
    UPLOAD_DIR=/upload \
    SESSION_SAVE_PATH=/tmp/sessions \
    NGINX_VERSION=1.10.0 \
    PHP56_VERSION=5.6.21 \
    PATH=/usr/local/php/bin:$PATH

# BUILD PHP, nginx and other dependencies.
COPY build-scripts /build-scripts

RUN /bin/bash /build-scripts/apt_build_deps.sh install && \
    /bin/bash /build-scripts/import_pgp_keys.sh && \
    /bin/bash /build-scripts/build_nginx.sh && \
    /bin/bash /build-scripts/build_php56.sh


# BUILD PHP C extensions & add to php.ini
    
RUN pecl install mongo && echo "extension=mongo.so" > $PHP56_DIR/lib/conf.d/mongo.ini
RUN pecl install intl && echo "extension=intl.so" > $PHP56_DIR/lib/conf.d/intl.ini
RUN pecl install gender && echo "extension=gender.so" > $PHP56_DIR/lib/conf.d/gender.ini
RUN echo "extension=curl.so" > $PHP56_DIR/lib/conf.d/curl.ini 
RUN echo "extension=gd.so" > $PHP56_DIR/lib/conf.d/gd.ini

EXPOSE 8080

# Lock down the web directories
RUN mkdir -p $APP_DIR $LOG_DIR $UPLOAD_DIR $SESSION_SAVE_PATH \
        $NGINX_USER_CONF_DIR \
    && chown -R www-data.www-data \
        $APP_DIR $UPLOAD_DIR $SESSION_SAVE_PATH $LOG_DIR \
        $NGINX_USER_CONF_DIR \
    && chmod 755 $UPLOAD_DIR $SESSION_SAVE_PATH

# Put config files into place.
COPY nginx.conf fastcgi_params gzip_params $NGINX_DIR/conf/
COPY php.ini $PHP56_DIR/lib/php.ini
COPY php-fpm.conf $PHP56_DIR/etc/php-fpm.conf
RUN touch $PHP56_DIR/etc/php-fpm-user.conf
COPY supervisord.conf /etc/supervisor/supervisord.conf
COPY logrotate.app_engine /etc/logrotate.d/app_engine



